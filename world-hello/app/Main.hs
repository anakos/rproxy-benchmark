module Main where

import           Lib
import           Magicbane
import           Metrics
import           RIO

main
  :: IO ()
main = do
  let config = defConfig

  (_, modLogg) <- newLogger (LogStderr defaultBufSize) simpleFormatter
  metrStore <- serverMetricStore <$> forkMetricsServer (config ^. mcBind) (config ^. mcPort)
  metrWai   <- registerWaiMetrics metrStore -- This one for the middleware
  modMetr   <- newMetricsWith metrStore -- And this one for the Magicbane app (for timed/gauge/etc. calls in your actions)

  modHc <- newHttpClient

  let ctx = (modLogg, modMetr, modHc, config)

  let waiMiddleware = metrics metrWai -- you can compose it with other middleware here
  defWaiMain $ waiMiddleware $ magicbaneApp api EmptyContext ctx $ world
