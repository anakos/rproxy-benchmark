{ ghc }:
let
  pkgs =
    import ./nixpkgs.nix { };
in
  with pkgs; haskell.lib.buildStackProject {
    inherit ghc;
    name = "world-hello";
    buildInputs = [ zlib git ];
  }
