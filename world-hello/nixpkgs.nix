opts:
let
  pinnedVersion =
    builtins.fromJSON (builtins.readFile ../.nixpkgs-version.json);
  pinnedPkgs =
    import (builtins.fetchGit {
      inherit (pinnedVersion) url rev;
      ref = "nixos-unstable";
    });
in
  pinnedPkgs opts
