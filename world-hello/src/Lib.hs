{-# LANGUAGE DeriveGeneric, TypeApplications #-}

module Lib where

import qualified Metrics as Metrics
import           Magicbane
import           RIO

type WorldRoute = "world" :> Get '[PlainText] Text
type API = WorldRoute

type Ctx = (ModLogger, ModMetrics, ModHttpClient, Metrics.Config)
type App = RIO Ctx

api
  :: Proxy API
api =
  Proxy @API  

world
  :: App Text
world =
  return "hello!"

