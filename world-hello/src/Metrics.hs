{-# LANGUAGE DeriveGeneric   #-}
{-# LANGUAGE TemplateHaskell #-}

module Metrics where

import           Control.Lens.TH (makeLenses)
import           Magicbane
import           RIO

data Config = Config'
  { _mcPort :: !Int
  , _mcBind :: !ByteString
  } deriving (Generic, Show)

makeLenses ''Config

instance DefConfig Config where
  defConfig =
    Config' 9090 "0.0.0.0"

instance FromEnv Config where
  fromEnv =
    gFromEnvCustom Option { dropPrefixCount = 0, customPrefix = "WORLD_HELLO" }
