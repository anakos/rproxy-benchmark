let
  pinnedVersion =
    builtins.fromJSON (builtins.readFile ./.nixpkgs-version.json);
  enableMozillaOverlay =
    true;
  mozilla-overlay =
    import
    (
      builtins.fetchTarball
      https://github.com/mozilla/nixpkgs-mozilla/archive/master.tar.gz
    );
  cfg = 
    {
      config.allowUnfree = true;
      overlays           = [ mozilla-overlay (import ./vscode.nix) ];
    };
  pinnedPkgs =
    import (builtins.fetchGit {
      inherit (pinnedVersion) url rev;
      ref = "nixos-unstable";
    }) cfg;

  rustNightlyChannel = (pinnedPkgs.rustChannelOf { date = "2019-09-25"; channel = "nightly"; });
  
  haskell865 =
    pinnedPkgs.haskell.packages.ghc865.ghcWithHoogle (
          haskellPackages: with haskellPackages; [
            aeson
            array
            arrows
            async
            binary
            bytestring
            conduit
            conduit-extra
            criterion
            http-conduit
            xml-conduit
            containers
            criterion
            deepseq
            hedgehog
            hspec
            hspec-core
            lens
            lifted-async
            mtl
            optparse-applicative
            QuickCheck
            random
            rio
            req
            resourcet
            stm
            tasty
            tasty-hedgehog
            text
            turtle
            warp
            # tools
            cabal-install
            alex
            doctest
            ghcid
            happy
            happy
            hindent
            hlint
            stack
            stylish-haskell
            hasktags
            apply-refact
            cabal-install
          ]);
  
in

# This allows overriding pkgs by passing `--arg pkgs ...`
{ pkgs ? pinnedPkgs }:

with pkgs; mkShell {
  RUST_BACKTRACE = 1;
  RUST_SRC_PATH  = "${rustNightlyChannel.rust-src}/lib/rustlib/src/rust/src";
  RUST_DOCS      = "${rustNightlyChannel.rust-docs}/share/doc/rust/html/index.html";
  # TODO: why is SSL_CERT_FILE required in order to get cargo to work?
  SSL_CERT_DIR  = "/etc/ssl/certs";
  SSL_CERT_FILE = "/etc/ssl/certs/ca-certificates.crt";
  nativeBuildInputs = [
    # environment
    binutils
    gcc
    gnumake
    openssl
    pkgconfig
    git
    cacert
  ];
  
  buildInputs = [
    # environment
    binutils
    gcc
    gnumake
    openssl
    pkgconfig
    git
    gnuplot
    wrk2
    apacheHttpd
    # rust
    rustNightlyChannel.rust
    rustNightlyChannel.rust-src
    rustNightlyChannel.rust-docs
    vscode-with-extensions
    rustup
    # haskell
    haskell865
  ] ++
    pkgs.stdenv.lib.optionals pkgs.stdenv.isDarwin [
      pkgs.darwin.cf-private
      pkgs.darwin.Security
      pkgs.darwin.apple_sdk.frameworks.CoreServices
  ];
  shellHook = ''
    export CARGO_HOME="$(pwd)/.cargo";
    export RUSTUP_HOME="$(pwd)/.rustup";
  ''; 
}
