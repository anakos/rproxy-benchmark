use rproxy::MyResult;
use bytes::Bytes;
use futures::{
    select,
    TryStreamExt,
};
use std::{
    collections::hash_map::{Entry, HashMap},
    net::SocketAddr,
    ops::*,
};
use tokio::{
    codec::{BytesCodec, FramedRead, FramedWrite,},
    net::{TcpListener, TcpStream},
    prelude::*,
    sync::{mpsc, oneshot},
};
use tokio_io::{
    AsyncWrite as TokioAsyncWrite,
    AsyncWriteExt as TokioAsyncWriteExt,
};
use tokio_io::split::{split as split_stream, ReadHalf, WriteHalf};
use tracing::{error, trace, warn};

type Sender<T> = mpsc::UnboundedSender<T>;
type Receiver<T> = mpsc::UnboundedReceiver<T>;

type Source = FramedRead<ReadHalf<TcpStream>, BytesCodec>;

#[tokio::main]
async fn main() -> MyResult<()> {
    rproxy::init_tracing_subscriber(tracing_subscriber::filter::LevelFilter::WARN.into())?;

    let proxy_addr  = "127.0.0.1:9000".parse::<SocketAddr>()?;
    let target_addr = "127.0.0.1:3000".parse::<SocketAddr>()?;

    accept_loop(proxy_addr, target_addr)
        .await
}

async fn accept_loop(proxy_addr: SocketAddr, target_addr: SocketAddr) -> MyResult<()> {
    trace!("TCP Proxy Started");

    let listener = TcpListener::bind(proxy_addr).await?;
    
    let (server_broker_sender, server_broker_receiver) = mpsc::unbounded_channel();
    let server_broker_handle                           =
        spawn_and_log_error(broker_loop(server_broker_receiver));
    
    let mut incoming = listener.incoming().enumerate();

    while let Some((idx, stream)) = incoming.next().await {
        let client_stream   = stream?;
        let upstream_target = TcpStream::connect(target_addr).await?;

        let (mut client_reader, client_writer, ) = split_stream(client_stream);

        let (server_reader, mut server_writer, ) = split_stream(upstream_target);
        let server_reader = FramedRead::new(server_reader, BytesCodec::new());

        spawn_and_log_error(async move {
            client_reader.copy(&mut server_writer).await?;

            trace!("copied all client input to upstream server");

            Ok(())
        });

        spawn_and_log_error(connection_loop(idx, server_broker_sender.clone(), server_reader, client_writer,));
    }

    drop(server_broker_sender);
    let _ = server_broker_handle.0.await;
    
    warn!("completed server broker shutdown, exiting accept loop");

    Ok(())
}

async fn connection_loop(
    id: usize,
    mut broker: Sender<Event>,
    mut reader: Source,
    writer: impl TokioAsyncWrite + Send + Sync + Unpin +  'static
) -> MyResult<()> {
    let (_shutdown_sender, shutdown_receiver) = mpsc::unbounded_channel::<Void>();
    if let Err(err) = broker.send(Event::NewConnection { id, writer: Box::new(writer), shutdown: shutdown_receiver, }).await {
        error!("connection_loop: error [{}] creating new connection [id = {}]", err, id);
        return Err(err.into())
    } else {
        trace!("connection_loop: new connection registered [id = {}]", id);
    }

    while let Some(bm) = reader.try_next().await? {
        broker
            .send(Event::Data { id, bytes: Bytes::from(bm), })
            .await
            .unwrap();
    }
    trace!("connection_loop: terminating connection [id = {}]", id);
    Ok(())
}


/**
  Thread Stats   Avg      Stdev     Max   +/- Stdev
    Latency    53.29ms    6.30ms 105.73ms   98.49%
    Req/Sec   200.61     14.86   292.00     96.22%
  Latency Distribution (HdrHistogram - Recorded Latency)
 50.000%   52.48ms
 75.000%   53.06ms
 90.000%   53.53ms
 99.000%  103.36ms
 99.900%  105.02ms
 99.990%  105.41ms
 99.999%  105.79ms
100.000%  105.79ms

using tokio channels:
  Thread Stats   Avg      Stdev     Max   +/- Stdev
    Latency    53.52ms    6.29ms 105.92ms   98.44%
    Req/Sec   200.60     15.22   292.00     96.54%
  Latency Distribution (HdrHistogram - Recorded Latency)
 50.000%   52.80ms
 75.000%   53.25ms
 90.000%   53.66ms
 99.000%  103.49ms
 99.900%  104.89ms
 99.990%  105.54ms
 99.999%  105.98ms
100.000%  105.98ms

 */
async fn connection_writer_loop(
    id: usize,
    messages: &mut Receiver<Bytes>,
    mut client_writer: impl AsyncWrite + Unpin,
    mut shutdown: Receiver<Void>,
) -> MyResult<()> {
    trace!("connection_writer_loop: starting [id = {}]", id);
    loop {
        select! {
            msg = messages.next().fuse() => match msg {
                Some(data) => {
                    tokio_timer::delay_for(std::time::Duration::from_millis(50))
                        .await;
                    client_writer.write_all(&data).await?
                },
                None => break,
            },
            void = shutdown.next().fuse() => match void {
                Some(void) => match void {},
                None => {
                    warn!("connection_writer_loop: shutdown received  [id = {}]", id);
                    break
                },
            }
        }
    }

    warn!("connection_writer_loop: terminating [id = {}]", id);

    Ok(())
}

async fn broker_loop(mut events: Receiver<Event>) -> MyResult<()> {
    let (disconnect_sender, mut disconnect_receiver) =
        mpsc::unbounded_channel::<(usize, Receiver<Bytes>)>();
    let mut peers: HashMap<usize, Sender<Bytes>> = HashMap::new();

    loop {
        let event = select! {
            event = events.next().fuse() => match event {
                None => break,
                Some(event) => event,
            },
            disconnect = disconnect_receiver.next().fuse() => {
                let (name, _pending_messages) = disconnect.unwrap();
                assert!(peers.remove(&name).is_some());
                continue;
            },
        };
        match event {
            Event::Data { id, bytes, } => {
                if let Some(peer) = peers.get_mut(&id) {
                    peer.send(bytes).await.unwrap();
                }
            },
            Event::NewConnection {
                id,
                writer,
                shutdown,
            } => match peers.entry(id) {
                Entry::Occupied(..) => (),
                Entry::Vacant(entry) => {
                    let (client_sender, mut client_receiver) = mpsc::unbounded_channel();
                    entry.insert(client_sender);
                    let mut disconnect_sender = disconnect_sender.clone();
                    spawn_and_log_error(async move {
                        let res =
                            connection_writer_loop(id, &mut client_receiver, writer, shutdown)
                                .await;
                        disconnect_sender
                            .send((id, client_receiver))
                            .await
                            .unwrap();
                        res
                    });
                }
            },
        }
    }
    drop(peers);
    drop(disconnect_sender);
    while let Some((_name, _pending_messages)) = disconnect_receiver.next().await {}
    Ok(())
}

// utility functions
fn spawn_and_log_error<F>(fut: F) -> JoinHandle
where
    F: Future<Output = MyResult<()>> + Send + 'static,
{
    let (sender, receiver) = oneshot::channel::<Void>();
    tokio::spawn(async move {
        if let Err(e) = fut.await {
            error!("task error: {}", e);
        }
        drop(sender);
    });
    JoinHandle(receiver)
}

struct JoinHandle(oneshot::Receiver<Void>);

// types
#[derive(Debug)]
enum Void {}

enum Event {
    NewConnection {
        id: usize,
        writer: Box<dyn AsyncWrite + Send + Sync + Unpin + 'static>,
        shutdown: Receiver<Void>,
    },
    Data {
        id: usize,
        bytes: Bytes,
    },
}

// variants

/*
  Thread Stats   Avg      Stdev     Max   +/- Stdev
    Latency    53.40ms    5.88ms 115.84ms   98.61%
    Req/Sec   196.11     36.90   377.00     95.38%
  Latency Distribution (HdrHistogram - Recorded Latency)
 50.000%   52.70ms
 75.000%   53.25ms
 90.000%   53.73ms
 99.000%  104.19ms
 99.900%  108.93ms
 99.990%  115.46ms
 99.999%  115.90ms
100.000%  115.90ms

Error: Os { code: 24, kind: Other, message: "Too many open files" }
+ socket errors
*/
async fn _server_to_client(
    server_reader: Source,
    client_writer: FramedWrite<WriteHalf<TcpStream>, BytesCodec>,
) -> MyResult<()> {
    let server_reader = server_reader
        .map_ok(|bm|
            async move {
                tokio_timer::delay_for(std::time::Duration::from_millis(50))
                    .await;
                Ok(Bytes::from(bm))
            }
        )
        .try_buffer_unordered(100);
    server_reader.forward(client_writer)
        .await?;
    Ok(())
}

/*
  Thread Stats   Avg      Stdev     Max   +/- Stdev
    Latency     1.53ms  417.99us   6.04ms   68.34%
    Req/Sec   212.05     43.51   444.00     83.83%
  Latency Distribution (HdrHistogram - Recorded Latency)
 50.000%    1.51ms
 75.000%    1.79ms
 90.000%    2.07ms
 99.000%    2.56ms
 99.900%    3.80ms
 99.990%    5.08ms
 99.999%    6.04ms
100.000%    6.04ms
*/
async fn _accept_loop_direct_copy_1(proxy_addr: SocketAddr, target_addr: SocketAddr) -> MyResult<()> {
    trace!("TCP Proxy Started");

    let listener = TcpListener::bind(proxy_addr).await?;
    
    let (server_broker_sender, server_broker_receiver) = mpsc::unbounded_channel();
    let server_broker_handle                           =
        spawn_and_log_error(broker_loop(server_broker_receiver));
    
    let mut incoming = listener.incoming();

    while let Some(client_stream) = incoming.try_next().await? {
        let upstream_target = TcpStream::connect(target_addr).await?;

        let (mut client_reader, mut client_writer, ) = split_stream(client_stream);
        let (mut server_reader, mut server_writer, ) = split_stream(upstream_target);

        spawn_and_log_error(async move {
            client_reader.copy(&mut server_writer).await?;

            trace!("copied all client input to upstream server");

            Ok(())
        });


        spawn_and_log_error(async move {
            server_reader.copy(&mut client_writer).await?;

            trace!("copied all client input from upstream server");

            Ok(())
        });
    }

    drop(server_broker_sender);
    let _ = server_broker_handle.0.await;
    
    warn!("completed server broker shutdown, exiting accept loop");

    Ok(())
}
