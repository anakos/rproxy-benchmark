// use tracing::{debug, debug_span, info, warn};
// use tracing_attributes::instrument;
// use tracing_futures::Instrument;
use tracing_subscriber::{filter::Directive, EnvFilter, FmtSubscriber};

pub type MyResult<A> = Result<A, Box<dyn std::error::Error + Send + Sync>>;

pub fn init_tracing_subscriber(directive: Directive) -> MyResult<()> {
    let subscriber = FmtSubscriber::builder()
        .with_env_filter(EnvFilter::from_default_env().add_directive(directive))
        .finish();
    tracing::subscriber::set_global_default(subscriber)?;
    
    Ok(())
}
