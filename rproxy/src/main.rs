use rproxy::MyResult;

use async_std::{
    net::{TcpListener, TcpStream},
    prelude::*,
    task,
};
use bytes::Bytes;

use futures::{
    channel::mpsc,
    select,
    AsyncReadExt,
    FutureExt,
    SinkExt,
    TryStreamExt
};
use futures_codec::{FramedRead, FramedWrite, BytesCodec};
use std::{
    collections::hash_map::{Entry, HashMap},
    net::SocketAddr,
    ops::*,
};

use tracing::{error, trace, warn};

type Sender<T> = mpsc::UnboundedSender<T>;
type Receiver<T> = mpsc::UnboundedReceiver<T>;

type Source = FramedRead<futures::io::ReadHalf<TcpStream>, BytesCodec>;
type Destination = FramedWrite<futures::io::WriteHalf<TcpStream>, BytesCodec>;

// TODO: tracing should be configurable, set to ERROR by default, enable by flag
fn main() -> MyResult<()> {
    // rproxy::init_tracing_subscriber("rproxy=trace".parse()?)?;
    rproxy::init_tracing_subscriber(tracing_subscriber::filter::LevelFilter::WARN.into())?;

    let proxy_addr  = "127.0.0.1:9000".parse::<SocketAddr>()?;
    let target_addr = "127.0.0.1:3000".parse::<SocketAddr>()?;
    task::block_on(accept_loop(proxy_addr, target_addr))
}

async fn accept_loop(proxy_addr: SocketAddr, target_addr: SocketAddr) -> MyResult<()> {
    trace!("TCP Proxy Started");

    let listener = TcpListener::bind(proxy_addr).await?;
    
    let (server_broker_sender, server_broker_receiver) = mpsc::unbounded();
    let server_broker_handle                           = task::spawn(broker_loop(server_broker_receiver));
    
    let mut incoming = listener.incoming().enumerate();

    while let Some((idx, stream)) = incoming.next().await {
        let client_stream   = stream?;
        let upstream_target = TcpStream::connect(target_addr).await?;

        let (client_reader, client_writer, ) = futures::AsyncReadExt::split(client_stream);
        let client_writer = FramedWrite::new(client_writer, BytesCodec {});

        let (server_reader, mut server_writer, ) = futures::AsyncReadExt::split(upstream_target);
        let server_reader = FramedRead::new(server_reader, BytesCodec {});

        spawn_and_log_error(async move {
            client_reader.copy_into(&mut server_writer).await?;

            trace!("copied all client input to upstream server");

            Ok(())
        });

        spawn_and_log_error(connection_loop(idx, server_broker_sender.clone(), server_reader, client_writer,));
    }

    drop(server_broker_sender);
    let _ = server_broker_handle.await;
    
    warn!("completed server broker shutdown, exiting accept loop");

    Ok(())
}

async fn connection_loop(
    id: usize,
    mut broker: Sender<Event>,
    mut reader: Source,
    writer: Destination
) -> MyResult<()> {
    let (_shutdown_sender, shutdown_receiver) = mpsc::unbounded::<Void>();
    if let Err(err) = broker.send(Event::NewConnection { id, writer, shutdown: shutdown_receiver, }).await {
        error!("connection_loop: error [{}] creating new connection [id = {}]", err, id);
        return Err(err.into())
    } else {
        trace!("connection_loop: new connection registered [id = {}]", id);
    }

    while let Some(bm) = reader.try_next().await? {
        broker
            .send(Event::Data { id, bytes: Bytes::from(bm), })
            .await
            .unwrap();
    }
    trace!("connection_loop: terminating connection [id = {}]", id);
    Ok(())
}

async fn connection_writer_loop(
    id: usize,
    messages: &mut Receiver<Bytes>,
    mut client_writer: Destination,
    mut shutdown: Receiver<Void>,
) -> MyResult<()> {
    trace!("connection_writer_loop: starting [id = {}]", id);
    loop {
        select! {
            msg = messages.next().fuse() => match msg {
                Some(data) => {
                    futures_timer::Delay::new(std::time::Duration::from_millis(50))
                        .await?;
                    client_writer.send(data).await?
                },
                None => break,
            },
            void = shutdown.next().fuse() => match void {
                Some(void) => match void {},
                None => {
                    warn!("connection_writer_loop: shutdown received  [id = {}]", id);
                    break
                },
            }
        }
    }
    warn!("connection_writer_loop: terminating [id = {}]", id);

    Ok(())
}

async fn broker_loop(mut events: Receiver<Event>) {
    let (disconnect_sender, mut disconnect_receiver) =
        mpsc::unbounded::<(usize, Receiver<Bytes>)>();
    let mut peers: HashMap<usize, Sender<Bytes>> = HashMap::new();

    loop {
        let event = select! {
            event = events.next().fuse() => match event {
                None => break,
                Some(event) => event,
            },
            disconnect = disconnect_receiver.next().fuse() => {
                let (name, _pending_messages) = disconnect.unwrap();
                assert!(peers.remove(&name).is_some());
                continue;
            },
        };
        match event {
            Event::Data { id, bytes, } => {
                if let Some(peer) = peers.get_mut(&id) {
                    peer.send(bytes).await.unwrap();
                }
            },
            Event::NewConnection {
                id,
                writer,
                shutdown,
            } => match peers.entry(id) {
                Entry::Occupied(..) => (),
                Entry::Vacant(entry) => {
                    let (client_sender, mut client_receiver) = mpsc::unbounded();
                    entry.insert(client_sender);
                    let mut disconnect_sender = disconnect_sender.clone();
                    spawn_and_log_error(async move {
                        let res =
                            connection_writer_loop(id, &mut client_receiver, writer, shutdown)
                                .await;
                        disconnect_sender
                            .send((id, client_receiver))
                            .await
                            .unwrap();
                        res
                    });
                }
            },
        }
    }
    drop(peers);
    drop(disconnect_sender);
    while let Some((_name, _pending_messages)) = disconnect_receiver.next().await {}
}

// utility functions
fn spawn_and_log_error<F>(fut: F) -> task::JoinHandle<()>
where
    F: Future<Output = MyResult<()>> + Send + 'static,
{
    task::spawn(async move {
        if let Err(e) = fut.await {
            error!("task error: {}", e)
        }
    })
}

// types
#[derive(Debug)]
enum Void {}

enum Event {
    NewConnection {
        id: usize,
        writer: Destination,
        shutdown: Receiver<Void>,
    },
    Data {
        id: usize,
        bytes: Bytes,
    },
}
