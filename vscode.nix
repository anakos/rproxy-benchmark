self: super: {
  vscode-with-extensions =
    super.vscode-with-extensions.override {
      # When the extension is already available in the default extensions set.
      vscodeExtensions =
        with super.vscode-extensions; [ alanz.vscode-hie-server llvm-org.lldb-vscode ]
        ++ super.vscode-utils.extensionsFromVscodeMarketplace [
            {
                 name = "rust";
                 publisher = "rust-lang";
                 version = "0.6.3";
                 # sha256 = "0f66z6b374nvnrn7802dg0xz9f8wq6sjw3sb9ca533gn5jd7n297";
                 sha256 = "1r5q1iclr64wmgglsr3na3sv0fha5di8xyccv7xwcv5jf8w5rz5y";
            }
        ];
    };
}
